using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LabyrinthGames.Core;

namespace TOG.Managers
{
    public class EnemyManager : Singleton<EnemyManager>
    {
        [Header("References")]
        [SerializeField] private MazeSpawnPointsHelper mazeSpawnPoints;
        [SerializeField] private List<GameObject> enemiesPrefab;

        [Header("Settings")]
        [SerializeField] private int amountSpawnInit = 5;
        

        // [SerializeField] private List<GameObject> _instances;

        public void SpawnEnemies()
        {
            var spawnPoints = mazeSpawnPoints.GetSpawnPointRandomByCount(HeapProgression());

            for(int i = 0; i < spawnPoints.Count; i++)
            {
                GameObject instance = Instantiate(enemiesPrefab[Random.Range(0, enemiesPrefab.Count)]);
                instance.transform.localPosition = spawnPoints[i];
            }
        }

        public int HeapProgression()
        {
            return (int)Mathf.Ceil(amountSpawnInit + (amountSpawnInit * GameManager.Instance.enemiesProgression));
        }

        private void Start()
        {
            mazeSpawnPoints.OnSpawnPointsReady.AddListener(SpawnEnemies);
        }
    }
}
