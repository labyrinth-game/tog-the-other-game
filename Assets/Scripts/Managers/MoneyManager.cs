using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LabyrinthGames.Core;
using TMPro;
using TOG.Utils;

namespace TOG.Managers
{
    public class MoneyManager : Singleton<MoneyManager>
    {
        [Header("References")]
        [SerializeField] private TextMeshProUGUI moneyUI; // need added UI prefab Money

        [Header("Settings")]
        [SerializeField] private int funds = 1000;

        #region Flow Game
        public void Lose(int value)
        {
            funds -= value;
            UpdateRenderValue(funds);
        }
        
        public void Gain(int value)
        {
            funds += value;

            UpdateRenderValue(funds);
        }

        public bool IsOutOfMoney()
        {
            return funds <= 0;
        }
        #endregion

        public void UpdateRenderValue(int value)
        {
            moneyUI.text = StringFormater.Dot(value) + " F";
        }

        private void Start()
        {
            UpdateRenderValue(funds);
        }
    }

}