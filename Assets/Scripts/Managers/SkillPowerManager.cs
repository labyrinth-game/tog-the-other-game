﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LabyrinthGames.Core;
using TOG.SkillPowers;

namespace TOG.Managers
{
    public class SkillPowerManager : Singleton<SkillPowerManager>
    {
        [Header("References")]
        [SerializeField] private MazeSpawnPointsHelper mazeSpawnPoints;
        [SerializeField] private List<GameObject> skillPowersPrefab;
        private PlayerControls playerControls;

        [Header("Settings")]
        [SerializeField] private List<SkillPowerBase> skillAcquired;
        [SerializeField] private List<SkillPowerBase> skillSelected;
        [SerializeField] private int limitSkillSelected = 5;

        [SerializeField] private SkillPowerBase skillActive;

        [SerializeField] private int amountSpawnInit = 3;

        #region Skill Power Acquired
        public void AddAcquired(SkillPowerBase skill)
        {
            skillAcquired.Add(skill);
        }
        #endregion

        #region Skill Power Selected
        public void AddSelected(SkillPowerBase skill)
        {
            if (skillSelected.Count >= limitSkillSelected)
                return;

            skillSelected.Add(skill);
        }
        #endregion

        public void SpawnSkillPowers()
        {
            var spawnPoints = mazeSpawnPoints.GetSpawnPointRandomByCount(HeapProgression());

            for (int i = 0; i < spawnPoints.Count; i++)
            {
                GameObject instance = Instantiate(skillPowersPrefab[Random.Range(0, skillPowersPrefab.Count)]);
                instance.transform.localPosition = spawnPoints[i];
            }
        }

        public int HeapProgression()
        {
            return (int)Mathf.Ceil(amountSpawnInit + (amountSpawnInit * GameManager.Instance.skillPowerProgression));
        }

        public void Actions()
        {
            
            if (GameManager.Instance.moneyManager.IsOutOfMoney())
                return;

            Vector2 value = playerControls.Player.Skills.ReadValue<Vector2>();
            
            if (value == Vector2.left)
                skillSelected[0]?.Active();

            if (value == Vector2.up) // move right arrow
                skillSelected[1]?.Active();

            if (value == Vector2.right)
                skillSelected[2]?.Active();

            if (value == Vector2.down)
                skillSelected[3]?.Active();
        }

        #region Unity Events
        private void Update()
        {
            Actions();
        }
        private void Start()
        {
            if (mazeSpawnPoints != null)
                mazeSpawnPoints.OnSpawnPointsReady.AddListener(SpawnSkillPowers);
        }

        private void Awake()
        {
            playerControls = new PlayerControls();
        }

        private void OnEnable()
        {
            playerControls.Enable();
        }

        private void OnDisable()
        {
            playerControls.Disable();
        }
        #endregion
    }
}