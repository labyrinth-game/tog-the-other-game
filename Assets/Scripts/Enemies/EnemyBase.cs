using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TOG.Weapons;
using DG.Tweening;

namespace TOG.Enemies
{
    public class EnemyBase : MonoBehaviour
    {
        [Header("Base Settings")]
        [SerializeField] protected List<GameObject> dropItem;
        [SerializeField] protected float timeToEscape = 2f; // time to escape when running monster
        [SerializeField] protected float velocityToAttack = 2f;
        [SerializeField] protected float radiusToAttack = 4f;

        [SerializeField] protected float timeToWaitMoveAgain = 5f;
        [SerializeField] protected float radiusToMove = 5f;
        [SerializeField] protected float velocityWalking = 3f;

        [SerializeField] protected bool canStopMove = false;
        [SerializeField] protected bool hasAggressive = false;

        #region Overwrites
        protected virtual void Hit(float damage, GameObject target) { }
        protected virtual void StartMove() { }
        protected virtual void EndMove() { }

        protected virtual void Dead(int reward, GameObject target)
        {
            // drop it item
            var item = dropItem[Random.Range(0, dropItem.Count)];
            var instance = Instantiate(item, transform.position, Quaternion.identity);

            // move drop at target and after destroy objects
            instance.transform.DOMove(target.transform.position, .2f).SetEase(Ease.InBack).OnComplete(
                () => {
                    GameManager.Instance.moneyManager.Gain(reward);

                    Destroy(instance);
                });

            Destroy(gameObject);
        }

        protected virtual void StartAutoAttack(GameObject target) { }
        protected virtual void StopAutoAttack() { }
        #endregion

        #region Utils
        private Vector3 RandomPos()
        {
            var x = Random.Range(transform.position.x - radiusToMove, transform.position.x + radiusToMove);
            var z = Random.Range(transform.position.z - radiusToMove, transform.position.z + radiusToMove);

            return new Vector3(x, transform.position.y, z);
        }

        private void MoveEnemyAtTarget(GameObject target)
        {
            canStopMove = true; // stop walking random

            transform.DOLookAt(target.transform.position, .3f); // look to player

            var playerPos = target.transform.position;
            var pos = new Vector3(playerPos.x, transform.position.y, playerPos.z + 2f);
            transform.DOMove(pos, velocityToAttack);
        } 
        #endregion

        IEnumerator AutoMove()
        {
            while(!canStopMove)
            {
                var pos = RandomPos();

                transform.DOLookAt(pos, .3f);
                yield return new WaitForSeconds(.3f);

                StartMove();
                transform.DOMove(pos, velocityWalking).OnComplete(EndMove);

                yield return new WaitForSeconds(velocityWalking + timeToWaitMoveAgain);
            }
        }

        #region Collisions
        private void OnCollisionEnter(Collision other)
        {
            // stop movimentation when collider with wall and move to other position
            if (other.collider.tag == "Wall")
            {
                transform.DOKill();

                var pos = RandomPos();
                transform.DOLookAt(Vector3.back, .3f);
                transform.DOMove(pos, .4f);
            }

            // when player attack the enemy
            if (other.collider.tag == "Weapon" || other.collider.tag == "Projectile")
            {
                hasAggressive = true;
                MoveEnemyAtTarget(other.gameObject);
                StartAutoAttack(other.gameObject);

                float damage = GameManager.Instance.player.GetDamage();
                Hit(damage, other.gameObject);
            }       
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.transform.CompareTag("Player") && hasAggressive)
            {
                MoveEnemyAtTarget(other.gameObject);
                StartAutoAttack(other.gameObject);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.transform.CompareTag("Player"))
            {
                canStopMove = false; // start walking random
                StopAutoAttack();
            }
        }
        #endregion

        private void Start()
        {
            StartCoroutine(AutoMove());

            SphereCollider detection = GetComponent<SphereCollider>();

            if (detection != null)
            {
                detection.radius = radiusToAttack;
            }
            else
                Debug.LogWarning("Need SphereCollider with trigger to run at player and attack", gameObject);

        }
    }
}
