using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace TOG.Enemies
{
    public class EnemyPartDrop : MonoBehaviour
    {
        [SerializeField] private float timeToMoveAtPlayer = .5f;

        private void OnTriggerEnter(Collider other)
        {
            if (other.transform.CompareTag("Player"))
            {
                var playerPosition = other.gameObject.transform.position;
                transform.DOMove(playerPosition, timeToMoveAtPlayer).SetEase(Ease.InBounce).OnComplete(() => Destroy(gameObject));
            }
        }
    }
}
