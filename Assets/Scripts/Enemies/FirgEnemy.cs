﻿using System.Collections;
using UnityEngine;
using UnityEngine.VFX;
using DG.Tweening;

namespace TOG.Enemies
{
    public class FirgEnemy : EnemyBase
    {
        [Header("References")]
        [SerializeField] private Animator animator;
        [SerializeField] private GameObject vfx;
        [SerializeField] private GameObject circleMagicPrefab;

        [Header("Settings")]
        [SerializeField] private int maxHp = 20;
        [SerializeField] private int hp = 20;
        [SerializeField] private int damage = 5;

        [SerializeField] private int reward = 30;

        [SerializeField] private float timeToAttackAgain = 3f;

        private Coroutine _autoAttackCoroutine;
        private GameObject _target;
        private GameObject _vfxInstance;
        private GameObject _circleMagic;

        #region Game Flow
        protected override void Hit(float damage, GameObject target)
        {
            hp -= (int)damage;

            if (hp < 0)
                base.Dead(reward, target);
        }

        protected override void StartMove()
        {
            animator.SetBool("Moviment", true);
        }

        protected override void EndMove()
        {
            animator.SetBool("Moviment", false);
        }

        protected override void StartAutoAttack(GameObject target)
        {
            // save target
            _target = target;

            _autoAttackCoroutine = StartCoroutine(AutoAttack());
        }
        public void Attack()
        {
            // set size "polem" area at attack actived
            if (_vfxInstance == null)
            {
                _circleMagic = Instantiate(circleMagicPrefab, transform);
                _circleMagic.transform.position = Vector3.zero;
                _circleMagic.transform.localPosition = Vector3.up * .17f;
                _circleMagic.transform.DetachChildren();
                _circleMagic.GetComponent<Effects.CircleMagic>().SetSize((int)this.radiusToAttack * 2);

                _vfxInstance = Instantiate(vfx, transform);
                _vfxInstance.transform.DetachChildren();

                _vfxInstance.GetComponent<VisualEffect>().SetFloat("RangeAttack", base.radiusToAttack);
                _vfxInstance.GetComponent<VisualEffect>().Play();

            }
            animator.SetBool("Attack", true);

            // effect if attack a player
            if (_target.transform.CompareTag("Player"))
            {
                _target.GetComponent<PlayerController>().Hit(damage);
            }
        }

        protected override void StopAutoAttack()
        {
            StopCoroutine(_autoAttackCoroutine);
            _autoAttackCoroutine = null;

            _target = null;

            animator.SetBool("Attack", false);

            if (_vfxInstance != null)
            {
                Destroy(_vfxInstance);
                _vfxInstance = null;
            }

            if (_circleMagic != null)
            {
                _circleMagic.GetComponent<Effects.CircleMagic>().DestroyItem();
                _circleMagic = null;
            }
        }
        #endregion


        IEnumerator AutoAttack()
        {
            while (_target != null)
            {
                Attack();
                yield return new WaitForSeconds(timeToAttackAgain);
            }
        }

        #region Unity Events
        private void Awake()
        {
            if (animator == null)
                animator = GetComponent<Animator>();
        }

        private void Update()
        {
            if (base.hasAggressive && _target != null)
                transform.DOLookAt(_target.transform.position, 1f).SetEase(Ease.InBounce);
        }
        #endregion
    }
}