﻿using System.Collections;
using UnityEngine;

namespace TOG.Enemies
{
    public class ProtoEnemy : EnemyBase
    {
        [Header("Settings")]
        [SerializeField] private int maxHp = 100;
        [SerializeField] private int hp = 100;
        [SerializeField] private float damage = 10;

        [SerializeField] private int reward = 200;

        protected override void Hit(float damage, GameObject target)
        {
            hp -= (int)damage;

            if (hp < 0)
                base.Dead(reward, target);
        }
    }
}