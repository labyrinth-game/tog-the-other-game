using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using LabyrinthGames.Core;

namespace TOG.Events
{
    public class AnimationEvents : Singleton<AnimationEvents>
    {
        [Header("Callbacks")]
        public UnityEvent<string> OnAnimationFinish;

        [Header("Settings")]
        public List<string> animationsNames;

        public void AnimationFinish(string animationName)
        {
            OnAnimationFinish?.Invoke(animationName);
        }
    }
}
