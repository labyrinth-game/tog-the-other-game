using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using LabyrinthGames.Generators;
using LabyrinthGames.Core;
using DG.Tweening;

namespace TOG.Maze
{
    public class MazeRender : Singleton<MazeRender>
    {
        [Header("References")]
        [SerializeField] private MazeGenerator mazeGenerator;
        [SerializeField] private MazeSpawnPointsHelper mazeSpawnPoints;
        [SerializeField] private GameObject prefabWall;
        [SerializeField] private GameObject prefabDome;

        [Header("Settings")]
        [Tooltip("You must set size X of Wall")]
        [SerializeField] private float offset = 5f;
        [SerializeField] private int size = 10;

        [Header("Callbacks")]
        public UnityEvent OnFinishRender;

        private List<GameObject> _instances;
        private GameObject _domeInstance;
        private GameObject _containerWallsHorizontal;
        private GameObject _containerWallsVertical;

        #region Flow Game
        public void Render()
        {
            if (mazeGenerator == null)
            {
                Debug.LogWarning("Not found Maze Gernerator on component");
                return;
            }

            var (verticalWall, horizontalWall) = mazeGenerator.Generate(size);

            _containerWallsHorizontal = transform.Find("WallsHorizontal").gameObject;
            _containerWallsVertical = transform.Find("WallsVertical").gameObject;

            // positions for all labytinth
            var centerX = (size * offset) / 2;
            var centerZ = (size * offset) / 2;

            _containerWallsHorizontal.transform.position = Vector3.zero;
            _containerWallsHorizontal.transform.localPosition = new Vector3(centerX * -1, 0, centerZ * -1);

            _containerWallsVertical.transform.position = Vector3.zero;
            _containerWallsVertical.transform.localPosition = new Vector3(centerX * -1, 0, centerZ * -1);

            // dome
            CreateDome();

            // up all wall
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                {
                    if (verticalWall[x, y])
                        CreateWall(x, y, true);

                    if (horizontalWall[x, y])
                        CreateWall(x, y);
                }


            mazeSpawnPoints.GetSpawnPoints(size, offset);

            PlayerController.Instance?.Spawn(new Vector3(centerX / (size / 2) * -1, 0, centerZ / (size / 2) * -1));
        }


        private void CreateWall(int x, int z, bool hasVertical = false)
        {
            var container = hasVertical ? _containerWallsVertical : _containerWallsHorizontal;

            GameObject instance = Instantiate(prefabWall, container.transform);
            
            var horizontalPos = instance.transform.localScale.x * x;
            var verticalPos = instance.transform.localScale.x * z;
            var height = instance.transform.localScale.y / 2;

            instance.transform.localPosition = new Vector3(horizontalPos, height, verticalPos);
            
            if (hasVertical)
            {
                instance.transform.rotation = Quaternion.Euler(Vector3.up * -90);
                instance.transform.localPosition += Vector3.left * (offset / 2); // move to forward half of the size wall
            } else
            {
                instance.transform.localPosition += Vector3.back * (offset / 2); // move to forward half of the size wall
            }

            _instances.Add(instance);
        }

        private void CreateDome()
        {
            var centerX = (size * offset) / 2;
            var centerZ = (size * offset) / 2;

            GameObject instance = Instantiate(prefabDome, transform);
            instance.transform.position = Vector3.zero;
            instance.transform.localPosition = Vector3.zero;
            instance.transform.localScale *= size * offset;

            _domeInstance = instance;
        }
        #endregion

        #region Gets/Sets
        public int GetSize()
        {
            return size;
        }

        public float GetOffset()
        {
            return offset;
        }
        #endregion

        #region Unity Events
        private void Awake()
        {
            _instances = new List<GameObject>();
        }

        private void Start()
        {
            Render();
        }

        private void OnValidate()
        {
            if (mazeGenerator == null)
                mazeGenerator = GetComponent<MazeGenerator>();
            
            if (mazeSpawnPoints == null)
                mazeSpawnPoints = GetComponent<MazeSpawnPointsHelper>();
        }
        #endregion
    }
}
