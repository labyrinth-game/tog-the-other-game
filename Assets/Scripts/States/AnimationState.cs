using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TOG.State
{
    public class AnimationState : MonoBehaviour
    {
        [Header("References")]
        public Animator animator;

        [Header("Settings")]
        public List<AnimationStateType> animationList;

        private AnimationStateType _actualAnimation = new AnimationStateType(AnimationType.Idle);

        #region Sets
        public void SetState(AnimationType type)
        {
            _actualAnimation = getTriggerByType(type);
            animator.SetTrigger(_actualAnimation.trigger);
        }

        public void SetState(AnimationType type, float value)
        {
            _actualAnimation = getTriggerByType(type);
            animator.SetFloat(_actualAnimation.trigger, value);
        }

        public void SetState(AnimationType type, int value)
        {
            _actualAnimation = getTriggerByType(type);
            animator.SetInteger(_actualAnimation.trigger, value);
        }

        public void SetState(AnimationType type, bool boolean)
        {
            var newAnimation = getTriggerByType(type);

            newAnimation.boolean = boolean;
            _actualAnimation = newAnimation;
            animator.SetBool(newAnimation.trigger, newAnimation.boolean);
        } 
        #endregion

        public bool IsCurrentAnimation(AnimationType type)
        {
            return _actualAnimation.type == type;
        }

        public bool IsCurrentAnimation(AnimationType type, bool boolean)
        {
            return _actualAnimation.type == type && _actualAnimation.boolean == boolean;
        }

        public AnimationStateType getTriggerByType(AnimationType type)
        {
            return animationList.Find(i => i.type == type);
        }

        public AnimationStateType getTriggerByName(string name)
        {
            return animationList.Find(i => i.trigger == name);
        }
    }

    public enum AnimationType
    {
        Jump,
        Walking,
        Idle,
        Gathering,
        Dead,
        Running,
        Attack,
        Falling,
        Dash,
        Moviment,
        Hit
    }

    [System.Serializable]
    public class AnimationStateType
    {
        public AnimationType type;
        public string trigger;
        public bool boolean;

        public AnimationStateType(AnimationType type)
        {
            this.type = type;
        }
    }
}
