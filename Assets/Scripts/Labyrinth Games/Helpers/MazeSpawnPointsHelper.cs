using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using LabyrinthGames.Core;

public class MazeSpawnPointsHelper : Singleton<MazeSpawnPointsHelper>
{
    [SerializeField] private List<Vector3> spawnPositions;
    [SerializeField] private int amountSpawn = 10;

    [Header("Callbacks")]
    public UnityEvent OnSpawnPointsReady;

    private bool[,] _hasOccupied;

    public void GetSpawnPoints(int size, float offset)
    {
        _hasOccupied = new bool[size, size];

        for (var i = 0; i < amountSpawn; i++)
        {
            int x = Random.Range(0, size);
            int y = Random.Range(0, size);

           while (!_hasOccupied[x, y])
            {
                _hasOccupied[x, y] = true;

                var center = ((size * offset) / 2);
                var x_ = (x * offset - center) * -1 - offset / 2;
                var z_ = (y * offset - center) * -1 - offset / 2;

                spawnPositions.Add(new Vector3(x_, 0, z_));
            }
        }

        OnSpawnPointsReady?.Invoke();
    }

    #region Gets/Sets
    public List<Vector3> GetSpawnPositions()
    {
        return spawnPositions;
    }

    // this method return amount spawn point basead on count
    public List<Vector3> GetSpawnPointRandomByCount(int count)
    {
        if (spawnPositions.Count == 0)
            return null;

        if (count > spawnPositions.Count && spawnPositions.Count > 0)
            count = spawnPositions.Count;
        
        var newList = new List<Vector3>();

        for(var i = 0; i < count; i++)
        {
            var index = Random.Range(0, spawnPositions.Count);
            
            newList.Add(spawnPositions[index]); // extract item selected 

            spawnPositions.RemoveAt(index); // remove at list original
        }

        return newList;
    }

    public Vector3 GetCenterSpawn(int size, float offset)
    {
        return new Vector3(Mathf.Ceil((size * offset) / 2) - 1, 0, Mathf.Ceil((size * offset) / 2) - 1);
    }
    #endregion


    #region Unity Event
    private void Awake()
    {
        spawnPositions = new List<Vector3>();
    } 
    #endregion
}
