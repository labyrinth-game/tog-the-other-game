using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using LabyrinthGames.Core;

/*
 * This algoriothm is Aldous-Broder https://weblog.jamisbuck.org/2011/1/17/maze-generation-aldous-broder-algorithm
 * **/
namespace LabyrinthGames.Generators
{
    public class MazeGenerator : Singleton<MazeGenerator>
    {
        [SerializeField] private bool[,] verticalWalls;
        [SerializeField] private bool[,] horizontalWalls;

        private int _unvisited = 0;
        private int size;
        private bool[,] _visited;

        public (bool[,] verticalWall, bool[,] horizontalWall) Generate(int size)
        {
            InicializeParams(size);

            int[] p = { 0, 0 };
            Visit(p);

            while (_unvisited < size * size)
            {
                var next = PickNeighbor(p);

                if (!IsVisited(next))
                {
                    Visit(next);
                    RemoveWall(p, next);
                }
                p = next;
            }

            return (verticalWalls, horizontalWalls);

        }

        #region Flow Algorithm
        private void InicializeParams(int size)
        {
            _visited = new bool[size, size];
            horizontalWalls = new bool[size, size];
            verticalWalls = new bool[size, size];
            this.size = size;

            SetWallValueDefault();
        }

        private void Visit(int[] p)
        {
            _visited[p[0], p[1]] = true;
            _unvisited++;
        }

        private int[] PickNeighbor(int[] p)
        {
            var x = p[0];
            var y = p[1];

            int[,] neighbors = new int[,]
            {
                {x - 1, y },
                {x + 1, y },
                {x, y - 1 },
                {x, y + 1 },
            };

            return PickValid(neighbors);
        }

        private int[] Pick(int[,] neighbors)
        {
            var x = Random.Range(0, 4);

            return Enumerable.Range(0, 2).Select(y => neighbors[x, y]).ToArray();
        }

        private bool CheckValid(int[] point)
        {
            bool x = 0 <= point[0] && point[0] < size;
            bool y = 0 <= point[1] && point[1] < size;

            return x && y;
        }

        private int[] PickValid(int[,] neighbors)
        {
            var n = Pick(neighbors);
            bool b = CheckValid(n);

            while (!b)
            {
                n = Pick(neighbors);
                b = CheckValid(n);
            }

            return n;
        }

        private bool IsVisited(int[] p)
        {
            var x = p[0];
            var y = p[1];

            return _visited[x, y];
        }

        private void RemoveWall(int[] currentPosition, int[] nextPosition)
        {
            int dx = nextPosition[0] - currentPosition[0];
            int dy = nextPosition[1] - currentPosition[1];

            var x = currentPosition[0];
            var y = currentPosition[1];

            if (dy == -1)
                horizontalWalls[x, y] = false;

            if (dx == -1)
                verticalWalls[x, y] = false;

            if (dx == 1)
                verticalWalls[x + 1, y] = false;

            if (dy == 1)
                horizontalWalls[x, y + 1] = false;
        }

        private void SetWallValueDefault(bool val = true)
        {
            for(int x = 0; x < size; x++)
                for(int y = 0; y < size; y++)
                {
                    horizontalWalls[x, y] = val;
                    verticalWalls[x, y] = val;
                }
                    
        }
        #endregion
    }
}
