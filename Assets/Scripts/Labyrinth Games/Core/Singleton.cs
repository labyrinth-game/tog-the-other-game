using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LabyrinthGames.Core
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static T Instance = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = GetComponent<T>();
            else
                Destroy(gameObject);
        }
    }
}