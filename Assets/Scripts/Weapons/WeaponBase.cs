using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TOG.Weapons
{
    public class WeaponBase : MonoBehaviour
    {
        [SerializeField] protected float incrementDamage = 0; // percent damage
        public virtual float GetDamage() 
        {
            return incrementDamage;
        }
    }
}
