using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TOG.Weapons
{
    public class BatWeapon : WeaponBase
    {
        private void Start()
        {
            base.incrementDamage = .01f; // added 1% of the damage additional
        }
    }
}
