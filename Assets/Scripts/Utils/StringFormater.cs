using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace TOG.Utils
{
    public class StringFormater : MonoBehaviour
    {
        public static string Dot(int value)
        {
            CultureInfo elGR = CultureInfo.CreateSpecificCulture("el-GR");
            return value.ToString("0,0", elGR);
        }
    }
}
