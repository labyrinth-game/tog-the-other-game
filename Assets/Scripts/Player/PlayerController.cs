using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LabyrinthGames.Core;
using TOG.Maze;
using TOG;
using DG.Tweening;

namespace TOG
{
    public class PlayerController : Singleton<PlayerController>
    {
        #region Public Variables
        [Header("References")]
        [SerializeField] private PlayerAnimations animations;
        [SerializeField] private PlayerControls playerControls;
        [SerializeField] GameObject meshRender; // used how reference to skills animations

        [Space()]
        [Header("Settings")]
        [SerializeField] private float damage = 5f;
        [SerializeField] private float weaponIncrementWeapon = 0;
        [SerializeField] private float skillIncrement = 0;

        [SerializeField] private float speed = 4f;
        [SerializeField] private float fastSpeed = 8f;

        [SerializeField] private float rotationVelocity = 100f;

        [Space()]
        [Header("Animations")]
        [SerializeField] private float timeToAttackAgain = .2f;
        #endregion

        #region Private Variables
        private float _currentSpeed;
        private float _runningTime = 0;
        private bool _freezingMove = false;
        private bool _isRunning = false;
        #endregion

        #region Game Flow
        public void Spawn(Vector3 position)
        {
            transform.position = position;
        }

        public void Moviment()
        {
            if (_freezingMove)
                return;

            if (playerControls.Player.Run.triggered)
                _isRunning = !_isRunning;

            if (_isRunning)
                _currentSpeed = fastSpeed;
            else
                _currentSpeed = speed;

            var pos = playerControls.Player.Move.ReadValue<Vector2>();
            var verticalPosition = pos[1];
            var horizontalPosition = pos[0];

            Vector3 forward = verticalPosition * transform.forward;
            Vector3 strafe = horizontalPosition * transform.right;
            var move = forward + strafe;

            if (move != Vector3.zero)
            {
                gameObject.transform.position += move * _currentSpeed * Time.deltaTime;
            }
            else
            {
                _isRunning = false;
                _currentSpeed = 0;
            }

            var camera = GameObject.FindGameObjectWithTag("MainCamera");

            if (camera == null)
                Debug.LogWarning("Not found TAG MainCamera to rotate player", gameObject);
            else
                transform.rotation = Quaternion.Euler(0, camera.transform.eulerAngles.y, 0);

            // animations
            var velocity = _currentSpeed / fastSpeed; // calc velocity to move animation player (0 at 1).
            animations.Movement(velocity);
        }

        public void Attack()
        {
            if (playerControls.Player.Attack.triggered && CanAttackAgain())
            {
                animations.Attack();
            }
        }

        public void Hit(int damage)
        {
            GameManager.Instance.moneyManager.Lose(damage);
            animations.Hit();
        }

        public void Jump()
        {
            if (playerControls.Player.Jump.triggered)
                animations.Jump();
        }
        #endregion

        #region Validators
        private bool CanAttackAgain()
        {
            if (_runningTime > timeToAttackAgain)
            {
                _runningTime = 0;
                return true;
            }

            return false;
        }
        #endregion

        #region Get/Set Stats
        public float GetDamage()
        {
            var skillDamage = damage * skillIncrement;
            var weaponDamage = damage * weaponIncrementWeapon;

            return damage + skillDamage + weaponDamage;
        }

        public void SetIncrementSkillDamage(float increment)
        {
            skillIncrement += increment;
        }

        public void SetFreezingMove(bool value)
        {
            _freezingMove = value;
        }

        public SkinnedMeshRenderer GetMesh()
        {
            return this.meshRender.GetComponent<SkinnedMeshRenderer>();
        }
        #endregion

        #region Unity events
        private void Update()
        {
            _runningTime += Time.time;

            Moviment();
            Attack();
            Jump();
        }

        private void Awake()
        {
            playerControls = new PlayerControls();
        }

        private void OnEnable()
        {
            playerControls.Enable();
        }

        private void OnDisable()
        {
            playerControls.Disable();
        }

        private void OnValidate()
        {
            if (animations == null)
                animations = GetComponent<PlayerAnimations>();
        }
        #endregion
    }
}