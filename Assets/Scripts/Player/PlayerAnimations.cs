using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TOG.State;

[RequireComponent(typeof(TOG.State.AnimationState))]
public class PlayerAnimations : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private TOG.State.AnimationState animationState;

    #region Flow Animations Player
    public void Movement(float speed)
    {
        animationState.SetState(AnimationType.Moviment, speed);
    }

    public void Attack()
    {
        TOG.PlayerController.Instance?.SetFreezingMove(true);
        var getAttack = Random.Range(0, 2);
        
        animationState.SetState(AnimationType.Attack, getAttack);
    }
    
    public void Jump()
    {
        animationState.SetState(AnimationType.Jump);
    }
    
    public void Hit()
    {
        animationState.SetState(AnimationType.Hit);
    }

    #endregion

    #region Events
    public void OnFinishAttackChangePosition(string animationName)
    {
        if (animationName == "Attack")
        {
            TOG.PlayerController.Instance?.SetFreezingMove(false);// not move when is attacking
            animationState.SetState(AnimationType.Attack, -1); // plugoff attacks
        }
    }
    #endregion


    #region Unity Events
    private void Start()
    {
        TOG.Events.AnimationEvents.Instance?.OnAnimationFinish.AddListener(OnFinishAttackChangePosition);
    }

    private void OnValidate()
    {
        if (animationState == null)
            animationState = GetComponent<TOG.State.AnimationState>();
    } 
    #endregion
}
