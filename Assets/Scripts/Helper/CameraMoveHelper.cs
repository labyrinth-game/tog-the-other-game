using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TOG.Helper
{
    public class CameraMoveHelper : MonoBehaviour
    {
        [Header("References")]
        public GameObject person;
        public GameObject camAttachmentPosition;

        [Header("Settings")]
        public bool lockMouse = false;

        private bool _stopMoveCam = false;
        private bool _isPause = false;

        public void IsLockMouse()
        {
            if (!lockMouse) return;

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        public void Freezing()
        {
            _stopMoveCam = true;

            Cursor.visible = !Cursor.visible; ;
            Cursor.lockState = CursorLockMode.None;
        }

        public void Unfreezing()
        {
            _stopMoveCam = false;

            IsLockMouse();
        }

        #region --- Unity Events ---
        private void Start()
        {
            IsLockMouse();
            //GameManager.Instance?.OnPauseEvent.AddListener(OnPause);
            //GameManager.Instance?.OnResumeEvent.AddListener(OnResume);
        }

        private void Update()
        {
            if (_stopMoveCam || _isPause)
                return;

        }
        #endregion

        #region Game Flow
        public void OnPause()
        {
            _isPause = true;
        }

        public void OnResume()
        {
            _isPause = false;
        }
        #endregion
    }
}