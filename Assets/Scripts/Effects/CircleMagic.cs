using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace TOG.Effects
{
    public class CircleMagic : MonoBehaviour
    {
        [SerializeField] private int size = 5;
        [SerializeField] private float velocity = 3f;

        #region Flow Game
        public void Create()
        {
            transform.DOScale(size, velocity).SetEase(Ease.InOutQuint);
        }
        public void DestroyItem()
        {
            transform.DOScale(0, velocity).SetEase(Ease.InOutQuint).OnComplete(() => Destroy(gameObject));
        }
        #endregion

        #region Set/Get
        public void SetSize(int size)
        {
            this.size = size;
        }

        public void SetVelocity(float velocity)
        {
            this.velocity = velocity;
        }
        #endregion

        private void Start()
        {
            Create();
        }
    }
}
