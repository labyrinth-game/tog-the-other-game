using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TOG.Scenery
{
    public class BoxItem : MonoBehaviour
    {
        [SerializeField] private Animator animator;

        private bool _hasOpened = false;

        private void OnTriggerEnter(Collider other)
        {
            if (!_hasOpened)
            {
                _hasOpened = true;

                animator.SetTrigger("Open");
            }
        }
       
        private void OnValidate()
        {
            if (animator == null)
                animator = GetComponent<Animator>();
        }
    }
}
