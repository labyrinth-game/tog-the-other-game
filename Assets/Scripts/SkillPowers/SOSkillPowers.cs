using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SOSkillPowers : ScriptableObject
{
    [Header("Informations")]
    public string title;
    public string description;

    [Header("Stats")]
    public int damage;
    [Tooltip("Percent used to increment any attribute, values 0 at 1")]
    public float percentIncrement;
    [Tooltip("Time in seconds, ex: 10 is equal 10s")]
    public float timeToStop;

    [Header("Values")]
    public int cost;
    public int costToUpgrade;
    public int costToUse;
}
