using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TOG.Managers;
using TOG.Effects;

namespace TOG.SkillPowers
{
    public class MusclesSkillPower : SkillPowerBase
    {
        [Header("References")]
        [SerializeField] private GameObject dialogPrefab;
        [SerializeField] private GameObject circleMagicPrefab;
        [SerializeField] private Material auraOutilineMaterial;

        [Space()]
        [Header("Settings")]
        [SerializeField] private SOSkillPowers settings;
        [SerializeField] private float distanceShowDialog = 1f;

        private GameObject _instance;
        private Material[] _oldMaterial;
        private GameObject _circleInstance;

        #region Flow Initial Skill buy
        protected override void Buy()
        {
            GameManager.Instance?.moneyManager.Lose(settings.cost);

            base.Buy();
        }

        protected override void ShowDialog(GameObject target)
        {
            var targetPos = transform.position + Vector3.up; // position powerup
            var targetFace = target.transform.forward; // front player
            var targetRotation = Quaternion.Euler(target.transform.eulerAngles.x, target.transform.eulerAngles.y - 180, target.transform.eulerAngles.z); // rotation front player

            var spawnPos = targetPos + targetFace * 2f;

            // show dialog
            _instance = Instantiate(dialogPrefab, spawnPos, targetRotation);
            _instance.GetComponent<SkillPowerDialog>().SetInfo(settings);

            _instance.transform.SetParent(transform);
            _instance.transform.DOScale(0, .4f).From().SetEase(Ease.InOutBounce);
        }

        protected override void DestroyDialog()
        {
            _instance.transform.DOScale(0, .4f).SetEase(Ease.InOutBounce).OnComplete(() => Destroy(_instance));
            transform.DOScale(0, .5f).SetEase(Ease.InOutBounce); // dont can destroy because this object is used when active at skill
        }
        #endregion


        #region Flow When Active
        IEnumerator ChangeMashMaterial()
        {
            SkinnedMeshRenderer meshRenderer = PlayerController.Instance.GetMesh();
            _oldMaterial = meshRenderer.materials;

            Material[] m = new Material[] { _oldMaterial[0], auraOutilineMaterial };

            meshRenderer.materials = m;

            yield return new WaitForSeconds(settings.timeToStop);

            meshRenderer.materials = new Material[] { _oldMaterial[0], _oldMaterial[1] };
        }

        public override void Active()
        {
            PlayerController.Instance?.SetIncrementSkillDamage(settings.percentIncrement);
            GameManager.Instance.moneyManager.Lose(settings.costToUse);

            _circleInstance = Instantiate(circleMagicPrefab, PlayerController.Instance.transform);
            _circleInstance.transform.position = PlayerController.Instance.transform.position + Vector3.up * .07f;

            //StartCoroutine(CircleAnimation());
            StartCoroutine(ChangeMashMaterial());

            Invoke(nameof(Deactive), settings.timeToStop);
        }

        public void Deactive()
        {
            _circleInstance.GetComponent<CircleMagic>().DestroyItem();
            PlayerController.Instance?.SetIncrementSkillDamage(-settings.percentIncrement);
        }

        #endregion
    }
}
