using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TOG.Managers;

namespace TOG.SkillPowers
{
    public class SkillPowerBase : MonoBehaviour
    {
        private bool _playerCanBuy = false;

        #region Overwrites
        protected virtual void ShowDialog(GameObject target) { }

        protected virtual void DestroyDialog() { }

        protected virtual void Buy() 
        {
            // added on manager
            SkillPowerManager.Instance?.AddSelected(this); // change after to added on acquirer

            DestroyDialog();
        }

        public virtual void Active() { }
        #endregion

        #region Unity Events
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.E) && _playerCanBuy)
                Buy();
        }

        public void OnTriggerEnter(Collider other)
        {
            if (other.transform.CompareTag("Player"))
            {
                ShowDialog(other.gameObject);
                _playerCanBuy = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.transform.CompareTag("Player"))
            {
                DestroyDialog();
                _playerCanBuy = false;
            }
        }
        #endregion
    }
}
