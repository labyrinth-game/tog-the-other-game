using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace TOG.SkillPowers
{
    public class SkillPowerDialog : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private TextMeshPro title;
        [SerializeField] private TextMeshPro description;
        [SerializeField] private TextMeshPro cost;
        [SerializeField] private TextMeshPro tip;

        public void SetInfo(SOSkillPowers settings)
        {
            // change velue by template string on dialog
            cost.text = cost.text.Replace("$cost$", settings.cost.ToString());

            description.text = description.text.Replace("$costToUpgrade$", settings.costToUpgrade.ToString());
            description.text = description.text.Replace("$costToUse$", settings.costToUse.ToString());
            description.text = description.text.Replace("$damage$", settings.damage.ToString());

            title.text = settings.title;

            tip.text = tip.text.Replace("$buy$", "E");
        }
    }
}
