using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LabyrinthGames.Core;
using TOG.Managers;

namespace TOG
{
    public class GameManager : Singleton<GameManager>
    {
        [Header("Managers")]
        public MoneyManager moneyManager;
        public SkillPowerManager skillPowerManager;
        public EnemyManager enemyManager;

        [Header("Player")]
        public PlayerController player;

        [Header("Settings Game")]
        public float enemiesProgression = .1f;
        public float skillPowerProgression = .1f;

        public int amountSpawnEnemies = 10;
        public int amountSpawnSkillPower = 10;

        [Header("Settings Player")]
        public float playerProgression = .1f;
    }
}
